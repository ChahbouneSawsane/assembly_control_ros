#pragma once

#include <ros/ros.h>

#include <string>

//! \brief Base class for machines present in the assembly cell
//!
//! \tparam StateMsgT The type of the message containing the machine state
//! \tparam InputMsgT The type of the message containing the machine input signals
//! \tparam CommandMsgT The type of the message containing the machine commands
//! \tparam OutputMsgT The type of the message containing the machine output signals
    //! \brief Construct a new Machine Controller given a node handle and a topic
    //!
    //! \param node The node controlling the machine
    //! \param topic The topic associated with the machine

    //! \brief Where the handling of the machine's i/o must be performed

    //! \brief Send a command message
    //!
    //! \param command_message The message to send

    //! \brief Send the output signals
    //!
    //! \param output_message The message to send


    //! \brief Get the last received received state message
    //!
    //! \return const StateMsgT& Message with the machine state

    //! \brief Get the last received input signals
    //!
    //! \return InputMsgT& Message with the input signals

    //! \brief Called when a new state message is available
    //!
    //! \param message The new state

    //! \brief Called when new input signals are available
    //!
    //! \param message The new input signals
   


template <typename InputMsgT, typename OutputMsgT>
class Controleur_class {
    public:
        Controleur_class(ros::NodeHandle node, const std::string& topic)
            : node_(node) {
            //  robot input
            input_publisher_ = node.advertise<InputMsgT>(
                topic + "/input", 10);
            // robot output
            output_subscriber_ = node.subscribe<OutputMsgT>(
                topic + "/output", 10, &Controleur_class::outputCallback, this);
        }

        virtual void process() = 0;

       // recevoir les outputs
        OutputMsgT& getOutputs() {
            return output_message_;
        }
        // envoyer les inputs
        void sendInputs(InputMsgT output_message) {
            input_publisher_.publish(output_message);
        }
   

    private:
        // Machines outputs callback functions
        void outputCallback(const typename OutputMsgT::ConstPtr& message) {
            output_message_ = *message;
        }

        ros::NodeHandle node_;
        ros::Publisher  input_publisher_;
        ros::Subscriber output_subscriber_;

        OutputMsgT output_message_;
};

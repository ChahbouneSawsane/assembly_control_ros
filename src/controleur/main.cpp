#include <ros/ros.h>

#include <assembly_control_ros/assembly_station_state.h>
#include <assembly_control_ros/assembly_station_command.h>
#include <assembly_control_ros/assembly_station_input.h>
#include <assembly_control_ros/assembly_station_output.h>

#include <assembly_control_ros/evacuation_conveyor_state.h>
#include <assembly_control_ros/evacuation_conveyor_command.h>
#include <assembly_control_ros/evacuation_conveyor_input.h>
#include <assembly_control_ros/evacuation_conveyor_output.h>

#include <assembly_control_ros/camera_state.h>
#include <assembly_control_ros/camera_command.h>
#include <assembly_control_ros/camera_input.h>
#include <assembly_control_ros/camera_output.h>

#include <assembly_control_ros/supply_conveyor_state.h>
#include <assembly_control_ros/supply_conveyor_command.h>
#include <assembly_control_ros/supply_conveyor_input.h>
#include <assembly_control_ros/supply_conveyor_output.h>

#include <assembly_control_ros/robot_state.h>
#include <assembly_control_ros/robot_command.h>
#include <assembly_control_ros/robot_input.h>
#include <assembly_control_ros/robot_output.h>

#include <common/machine_controller.hpp>
#include <common/Controleur_class.hpp>

class AssemblyStation : public Controleur_class<
					
                                        assembly_control_ros::assembly_station_input,
                                        
                                        assembly_control_ros::assembly_station_output> {
			public:
   				AssemblyStation(ros::NodeHandle node)
       				 : Controleur_class(node, "ASS") {}

	    		virtual void process() override {}
											};
class EvacuationConveyor : public Controleur_class<
					
					assembly_control_ros::evacuation_conveyor_input,
					
					assembly_control_ros::evacuation_conveyor_output> {
			  public:
				EvacuationConveyor(ros::NodeHandle node)
				 : Controleur_class(node, "EC")  {}

			  virtual void process() override {}
											};
 class Camera : public Controleur_class<
				
				assembly_control_ros::camera_input,
				
				assembly_control_ros::camera_output> {
	       public:
		     Camera(ros::NodeHandle node)
			: Controleur_class(node, "Camera") {}

	       virtual void process() override {}
								     };
class SupplyConveyor : public Controleur_class<
					
					assembly_control_ros::supply_conveyor_input,
					
					assembly_control_ros::supply_conveyor_output> {
		       public:
			   SupplyConveyor(ros::NodeHandle node)
				: Controleur_class(node, "Supply_Conveyor") {}

		      virtual void process() override {}
											};
class Robot : public Controleur_class<
				
				assembly_control_ros::robot_input,
		
				assembly_control_ros::robot_output> {
	      public:
		Robot(ros::NodeHandle node)
		      : Controleur_class(node, "robot") {}

	      virtual void process() override {}
								    };
class Controleur {

		public :
			Controleur(ros::NodeHandle node) : node_(node) {
			}
		void process() {

		Robot			robot(node_);
		EvacuationConveyor	evacuation(node_);
		SupplyConveyor		supply(node_);
		AssemblyStation		assembly(node_);
		Camera			camera(node_);

        assembly_control_ros::robot_input 		robot_input;
        assembly_control_ros::evacuation_conveyor_input	evacuation_input;
	assembly_control_ros::supply_conveyor_input	supply_input;
	assembly_control_ros::assembly_station_input	assembly_input;
	assembly_control_ros::camera_input		camera_input;


        auto& robot_output = robot.getOutputs();
	auto& evacuation_output = evacuation.getOutputs();
	auto& supply_output = supply.getOutputs();
	auto& assembly_output = assembly.getOutputs();
	auto& camera_output = camera.getOutputs();
	
	bool dem_Right_EC;


	switch (state_controleur_) {

	case StateControleur::Start:
		if(!robot_output.SC2) {

		//	start = false;   je ne peux pas la faire
			robot_input.Demande_Right = true;

			robot.sendInputs(robot_input);
			ROS_INFO ("[controleur] robot à Ass ");
			state_controleur_ = StateControleur ::Attendre_SC;
					}
		else if(robot_output.SC2 && robot_output.Arr_right1 && evacuation_output.EC_en_Marche) {

		//	start = false;
			robot_output.SC2 = false;
			robot_output.Arr_right1 = false;
			evacuation_output.EC_en_Marche = false;
			ROS_INFO("[controleur] robot à SC");
			state_controleur_ = StateControleur::Attendre_Piece;
								     }
		break;
	case StateControleur::Attendre_SC:
		if(robot_output.Arr_right1 && robot_output.SC2) {
			robot_output.Arr_right1 = false;
			robot_output.SC2 = false;
			ROS_INFO("[controleur] robot à SC");
			state_controleur_ = StateControleur::Attendre_Piece;
								}
	       break;
	case StateControleur::Attendre_Piece:
		if(camera_output.CAM_ResultReady) {

			camera_output.CAM_ResultReady= false;
			robot_input.demande_prise = true;
			robot.sendInputs(robot_input);
			ROS_INFO("[controleur] piece arrivee");
			state_controleur_ = StateControleur::Attendre_Prise;
						}
	case StateControleur::Attendre_Prise:
		if(robot_output.Piece_Prise) {

			robot_output.Piece_Prise = false;
			ROS_INFO("[contoleur] piece prise");
			state_controleur_ = StateControleur::Assembl_Evac;
					     }
	case StateControleur::Assembl_Evac:
		if(assembly_output.CP1 && camera_output.CAM_part1) {
			assembly_output.CP1 = false;
			camera_output.CAM_part1 = false;
			robot_input.GoLeft = true;
			robot.sendInputs(robot_input);
			ROS_INFO("[controleur] deplacement a gauche");
			state_controleur_ = StateControleur::AttendreASS;
								}
		else if(assembly_output.CP2 && camera_output.CAM_part2) {
			assembly_output.CP2 = false;
			camera_output.CAM_part2 = false;
			robot_input.GoLeft = true;
			robot.sendInputs(robot_input);
                        ROS_INFO("[controleur] deplacement a gauche");
			state_controleur_ = StateControleur::Attendre_Ass;
								     }
		else if(assembly_output.CP3 && camera_output.CAM_part3) {
			assembly_output.CP3 = false;
			camera_output.CAM_part3 = false;
			robot_input.GoLeft = true;
			robot.sendInputs(robot_input);
			ROS_INFO("[controleur] deplacement a gauche");
			state_controleur_ = StateControleur::Attendre_ASS;
								    }
		else if(!assembly_output.CP1 && camera_output.CAM_part1) {
			camera_output.CAM_part1 = false;
			ROS_INFO("[controleur] deplacement a droite");
			state_controleur_ = StateControleur::DemgoRight;
								      }
		else if(!assembly_output.CP2 && camera_output.CAM_part2) {
			camera_output.CAM_part2 = false;
			ROS_INFO("[controleur] deplacement a droite");
			state_controleur_ = StateControleur::DemgoRight;
											     }
		else if(!assembly_output.CP3 && camera_output.CAM_part3) {
			camera_output.CAM_part3 = false;
			ROS_INFO("[controleur] deplacement a droite");
			state_controleur_ = StateControleur::DemgoRight;
								     }
	break;
	case StateControleur::AttendreASS:
		if(robot_output.FinLeft) {
			robot_output.FinLeft = false;
			robot_input.Demande_ASSpart1 = true;
			robot.sendInputs(robot_input);
			ROS_INFO("[controleur] assemblage Part1");
			state_controleur_ = StateControleur::Assemb1;
					 }
	break;
	case StateControleur::Assemb1:
		if(robot_output.FinAss1) {
			robot_output.FinAss1 = false;
			robot_input.DemandeLach = true;
			robot.sendInputs(robot_input);
			ROS_INFO("[controleur] Part1 assemble");
			state_controleur_ = StateControleur::Attendre_Flach;
					 }

	break;

	case StateControleur::Attendre_Flach:
		if(robot_output.FinLach) {
			robot_output.FinLach = false;
			assembly_input.Ass1 = true;
			supply_input.Fin_EC_AS = true;
			assembly.sendInputs(assembly_input);
			supply.sendInputs(supply_input);
			ROS_INFO("[controleur] fin assemblement");
			state_controleur_ = StateControleur::Start;
					  }
	break;
	case StateControleur::Attendre_Ass:
		if(robot_output.FinLeft) {
			robot_output.FinLeft = false;
			robot_input.Demande_ASSpart2 = true;
			robot.sendInputs(robot_input);
			ROS_INFO("[controleur] robot a Ass");
			state_controleur_ = StateControleur::Assemb2;
					}
	break;
	case StateControleur::Assemb2:
		if(robot_output.FinAss2) {
			robot_output.FinAss2 = false;
			robot_input.DemandeLach = true;
			robot.sendInputs(robot_input);
			ROS_INFO("[controleur] assemblage Part2");
			state_controleur_ = StateControleur::Attendre_f_Lach;
					}
	break;
	case StateControleur::Attendre_f_Lach:
		if(robot_output.FinLach) {
			robot_output.FinLach = false;
			assembly_input.Ass2 = true;
			supply_input.Fin_EC_AS= true;
			assembly.sendInputs(assembly_input);
			supply.sendInputs(supply_input);
			ROS_INFO("[controleur] fin assemblement Part2");
			state_controleur_ = StateControleur::Start;
					  }
	break;
	case StateControleur::Attendre_ASS:
		if(robot_output.FinLeft) {
			robot_output.FinLeft = false;
			robot_input.Demande_ASSpart3 = true;
			robot.sendInputs(robot_input);
			ROS_INFO("[controleur] robot a Ass");
			state_controleur_ = StateControleur::Assemb3;
					  }
	break;
	case StateControleur::Assemb3:
		if(robot_output.FinAss3) {
			robot_output.FinAss3 = false;
			robot_input.DemandeLach = true;
			robot.sendInputs(robot_input);
			ROS_INFO("[controleur] assemblage Part3");
			state_controleur_ = StateControleur::Attendre_F_lach;
					  }
	break;

	case StateControleur::Attendre_F_lach:
		if(robot_output.FinLach) {
			robot_output.FinLach = false;
			assembly_input.Ass3 = true;
			supply_input.Fin_EC_AS= true;
			assembly.sendInputs(assembly_input);
			supply.sendInputs(supply_input);
			ROS_INFO("[controleur] fin assemblage Part3");
			state_controleur_ = StateControleur::Start;
					}
	break;
	case StateControleur::DemgoRight:
		
			
			evacuation_input.Piece_a_evacuer = true;
			evacuation.sendInputs(evacuation_input);
		        dem_Right_EC =true;
			ROS_INFO("[controleur] robot a EC");
		
			state_controleur_ = StateControleur::Attendre_Arr_EC;
			break;
		if(dem_Right_EC && !evacuation_output.ECArr)
		{
		robot_input.Demande_Right=true;
                robot.sendInputs(robot_input);
		}
	case StateControleur :: Attendre_Arr_EC:
		if(evacuation_output.ECArr)
		{
			
			evacuation_output.ECArr = false;			
			ROS_INFO("[controleur] Ec arrete");		
			state_controleur_= StateControleur :: Attendre_Fin_Evacu;
		}

		break;
	case StateControleur ::Attendre_Fin_Evacu:

		if(evacuation_output.EC_Marche) {
			evacuation_output.EC_Marche = false;

			state_controleur_ = StateControleur::Start;
						}
	break;
        }

       // sendCommands(commands);
    }

private:
	ros::NodeHandle node_;
  
    enum class StateControleur { Start, Attendre_SC, Attendre_Piece, Attendre_Prise, Assembl_Evac, AttendreASS, Assemb1, Attendre_F_lach, Fin_lachement, Attendre_Ass, Assemb2, Attendre_f_Lach, Attendre_ASS, Assemb3, Attendre_Flach, DemgoRight, dem_Right_EC, Attendre_Arr_EC, Attendre_Fin_Evacu, };

   // StateSupply state_supply_;
    //StateCamera	state_camera_;
   // StateEvacuation	state_evacuation_;
   // StateAssembly	state_assembly_;
   // StateRobot	state_robot_;
    StateControleur state_controleur_;
};

int main(int argc, char* argv[]) {
    ros::init(argc, argv, "controleur");

    ros::NodeHandle node;

    ros::Rate loop_rate(50); // 50 Hz

    Controleur controleur(node);

    while (ros::ok()) {
        controleur.process();

        ros::spinOnce();

        loop_rate.sleep();
    }
}

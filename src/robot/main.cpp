#include <ros/ros.h>

#include <assembly_control_ros/robot_state.h>
#include <assembly_control_ros/robot_command.h>
#include <assembly_control_ros/robot_input.h>
#include <assembly_control_ros/robot_output.h>

#include <common/machine_controller.hpp>

class Robot : public MachineController<assembly_control_ros::robot_state,
                                        assembly_control_ros::robot_input,
                                        assembly_control_ros::robot_command,
                                        assembly_control_ros::robot_output> {
public:
    Robot(ros::NodeHandle node)
        : MachineController(node, "robot"), state_(State::Pose) {
    }

	//declaration:

	int Position_robot =0;  //ASS = 0 ,  SC = 1 , EC = 2
	bool ASS = true,SC,SC1,EC,EC2,Arr_right,Deplacer_droite,Deplacer_gauche,Atteindre_pos_gauche;

    virtual void process() override {
        assembly_control_ros::robot_command commands;
        assembly_control_ros::robot_output outputs;

        auto& inputs = getInputs();

	switch (Position_robot) {

	case 0:
		if(Deplacer_droite && getState().at_supply_conveyor) {
			SC = true;
			SC1 = true;
			outputs.SC2 = true;
			ASS = false;
			Deplacer_droite = false;
				sendOuputs(outputs);				      }
		break;

	case 1:
		if(Deplacer_gauche && getState().at_assembly_station) {
			ASS = true;
			Atteindre_pos_gauche = true;
			SC = false;
	               Deplacer_gauche= false;
                       }								      
 			
		else if(Deplacer_droite && getState().at_evacuation_conveyor) {
			SC = false;
			EC = true;
			outputs.EC1 = true;
			EC2 = true;
			Deplacer_droite=false;			
	sendOuputs(outputs);					      }

		break;

	case 3:
		if(getState().at_supply_conveyor && Deplacer_gauche) {
			Deplacer_gauche = false;
			EC = false;
			SC = true;
			outputs.SC2 = true;
			Atteindre_pos_gauche = true;
				sendOuputs(outputs);				     }

		break;

		if(SC1 && !EC2) {
			SC1 = false;
			Arr_right = true;
			outputs.Arr_right1 = true;
			sendOuputs(outputs);
				}

		else if(EC2 && !SC1) {
			EC2 = false;
			Arr_right = true;
			outputs.Arr_right1 = true;
			sendOuputs(outputs);
				     }
			
							}

        switch (state_) {
        case State::Pose:
            if (inputs.demande_prise) {

		inputs.demande_prise = false;

                ROS_INFO("[robot] prend la piece ");
                state_ = State::ROB_GRASP;
             }
	    if (inputs.DemandeLach) {

		inputs.DemandeLach = false;


		ROS_INFO("[robot] lache la piece");
		state_ = State::ROB_RELEASE;
	     }
	    if (inputs.GoLeft) {

		inputs.GoLeft = false;

		Deplacer_gauche = true;
	

		ROS_INFO("[robot] se deplace a gauche");
		state_ = State::ROB_LEFT;
	     }
	    if (inputs.Demande_ASSpart3) {

		inputs.Demande_ASSpart3 = false;

		ROS_INFO("[robot] assemblement de part3");
		state_ = State::ROB_ASS_PART3;
	     }
	    if (inputs.Demande_ASSpart2) {

		inputs.Demande_ASSpart2 = false;


		ROS_INFO("[robot] assemblement de part2");
		state_ = State::ROB_ASS_PART2;
	     }
	    if(inputs.Demande_ASSpart1) {

		inputs.Demande_ASSpart1 = false;

		ROS_INFO("[robot] assemblement de part1");
		state_ = State::ROB_ASS_PART1;
	     }
	    if(inputs.Demande_Right) {

		inputs.Demande_Right = false;
		
		Deplacer_droite = true;
		

		ROS_INFO("[robot] deplacement a droite");
		state_ = State::ROB_RIGHT;
	      }

            break;
        case State::ROB_GRASP:

            	commands.grasp = true;
		sendCommands(commands);

            if (getState().part_grasped) {

                commands.grasp = false;
		outputs.Piece_Prise = true;

		sendOuputs(outputs);
		sendCommands(commands);

                ROS_INFO("[robot] fin prise");
                state_ = State::Pose;
            }
            break;
        case State::ROB_RELEASE:
		commands.release = true;
		sendCommands(commands);

            if (getState().part_released) {

		commands.release = false;
		outputs.FinLach = true;
		sendCommands(commands);
		sendOuputs(outputs);
		ROS_INFO("[robot] fin lachement");
                state_ = State::Pose;
		}
                break;
       case State::ROB_LEFT:
		commands.move_left = true;
		sendCommands(commands);

	    if (Atteindre_pos_gauche) {

		Atteindre_pos_gauche = false;
		outputs.FinLeft = true;
		
		sendOuputs(outputs);
		ROS_INFO("[robot] atteindre position gauche");
		state_ = State::Pose;
		}
		break;

            case State::ROB_ASS_PART3:
		commands.assemble_part3 = true;
		sendCommands(commands);
	    if (getState().part3_assembled) {

		commands.assemble_part3 = false;
		outputs.FinAss3 = true;
		sendCommands(commands);
		sendOuputs(outputs);
		ROS_INFO("[robot] assemblage Part3");
		state_ = State::Pose;
		}
		break;

          case State::ROB_ASS_PART2:
		commands.assemble_part2 = true;
		sendCommands(commands);
	    if (getState().part2_assembled) {

		commands.assemble_part2 = false;
		outputs.FinAss2 = true;
		sendCommands(commands);
		sendOuputs(outputs);
		ROS_INFO("[robot] assemblage Part2");
		state_ = State::Pose;
		}
		break;

	   case State::ROB_ASS_PART1:
		commands.assemble_part1 = true;
		sendCommands(commands);
	    if (getState().part1_assembled) {

		commands.assemble_part1 = false;
		outputs.FinAss1 = true;
		sendCommands(commands);
		sendOuputs(outputs);
		ROS_INFO("[robot] assemblage Part1");
		state_ = State::Pose;
		}
		break;

	   case State::ROB_RIGHT:
		commands.move_right = true;
		sendCommands(commands);
	     if (Arr_right) {

		commands.move_right = false;
		Arr_right = false;
		sendCommands(commands);
		ROS_INFO("[robot] atteindre position droite"); 
		state_ = State::Pose;
		}
		break;
        }

        sendCommands(commands);
    }

private:
    enum class State { Pose, ROB_GRASP, ROB_RIGHT, ROB_ASS_PART1, ROB_ASS_PART2, ROB_ASS_PART3, ROB_LEFT, ROB_RELEASE };

    State state_;
};

int main(int argc, char* argv[]) {
    ros::init(argc, argv, "robot");

    ros::NodeHandle node;

    ros::Rate loop_rate(50); // 50 Hz

    Robot robot(node);

    while (ros::ok()) {
        robot.process();

        ros::spinOnce();

        loop_rate.sleep();
    }
}

#include <ros/ros.h>

#include <assembly_control_ros/evacuation_conveyor_state.h>
#include <assembly_control_ros/evacuation_conveyor_command.h>
#include <assembly_control_ros/evacuation_conveyor_input.h>
#include <assembly_control_ros/evacuation_conveyor_output.h>

#include <common/machine_controller.hpp>


class EvacuationConveyor : public MachineController<assembly_control_ros::evacuation_conveyor_state,
                                        assembly_control_ros::evacuation_conveyor_input,
                                        assembly_control_ros::evacuation_conveyor_command,
                                        assembly_control_ros::evacuation_conveyor_output> {
public:
    EvacuationConveyor(ros::NodeHandle node)
        : MachineController(node, "evacuation"), state_(State::EC_ON) {
    }

    virtual void process() override {
        assembly_control_ros::evacuation_conveyor_command commands;
        assembly_control_ros::evacuation_conveyor_output outputs;

        auto& inputs = getInputs();
        switch (state_) {
        case State::EC_ON:
	commands.on=true;
sendCommands(commands);
            if (inputs.Piece_a_evacuer) {

		 commands.on = false;
		inputs.Piece_a_evacuer = false;
        	sendCommands(commands);
                ROS_INFO("[evacuation] debut evacuation");
                state_ = State::Demande_Arret;
			                }
            break;
        case State::Demande_Arret:

            if (getState().stopped && inputs.EC1) {
		 inputs.EC1 = false;
		 outputs.DemandeLach = true;
		 outputs.ECArr = true;

		sendOuputs(outputs);

		ROS_INFO("[evacuation] Lachement de la piece");
                state_ = State::Attendre_Lach;
					           }
            break;
        case State::Attendre_Lach:
            if (inputs.FinLach) {

                inputs.FinLach = false;
                outputs.GoLeft = true;
                outputs.Piece_Evac = true;
                outputs.Fin_EC_AS = true;

		sendOuputs(outputs);
		ROS_INFO("[evacuation] Fin evacuation");
		state_ = State::Fin_evacuation;
				}
           break;
       case State::Fin_evacuation:
	    if(inputs.FinLeft) {
		inputs.FinLeft = false;
		outputs.EC_Marche = true;
		outputs.EC_en_Marche= true;
		sendOuputs(outputs);
                ROS_INFO("[evacuation] Remise en marche du tapis");
		state_ = State::EC_ON;
        		        }
	break;


      }

        sendCommands(commands);
  }

private:
    enum class State { EC_ON, Demande_Arret, Attendre_Lach, Fin_evacuation };

    State state_;
};

int main(int argc, char* argv[]) {
    ros::init(argc, argv, "evacuation_conveyor");

    ros::NodeHandle node;

    ros::Rate loop_rate(50); // 50 Hz

    EvacuationConveyor Conveyor(node);

    while (ros::ok()) {
        Conveyor.process();

        ros::spinOnce();

        loop_rate.sleep();
    }
}

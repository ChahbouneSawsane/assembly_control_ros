#include <ros/ros.h>
#include <assembly_control_ros/assembly_station_state.h>
#include <assembly_control_ros/assembly_station_command.h>
#include <assembly_control_ros/assembly_station_input.h>
#include <assembly_control_ros/assembly_station_output.h>

#include <common/machine_controller.hpp>

class AssemblyStation
    : public MachineController<assembly_control_ros::assembly_station_state,
			       assembly_control_ros::assembly_station_input,
			       assembly_control_ros::assembly_station_command,
			       assembly_control_ros::assembly_station_output> {
public:
	AssemblyStation(ros::NodeHandle node)
    		: MachineController(node, "Assembly_Station"),state_(State::AS_STOP) {
	}

	virtual void process() override {
		assembly_control_ros::assembly_station_command commands;
		assembly_control_ros::assembly_station_output outputs;

//		assembly_control_ros::assembly_station_input inputs;
		auto& inputs = getInputs();
		switch (state_) {
		case State::AS_STOP:
	

		if((inputs.Ass1) && (inputs.Ass2) && (inputs.Ass3)) {

		// && outputs.not(CP1) && outputs.not(CP2) && outputs.not(CP3)) {

		inputs.Ass1 = false;
		inputs.Ass2 = false;
		inputs.Ass3 = false;

//		outputs.CP1 = true;
//		outputs.CP2 = true;
//		outputs.CP3 = true;

//		sendOutputs(outputs);

		ROS_INFO("[Assembly Station] CHECK");
		state_ = State::AS_CHECK;
	     }
	    break;
	case State::AS_CHECK:
	commands.check = true;
       sendCommands(commands);
		if(getState().valid) {

		   commands.check = false;
             	  sendCommands(commands);
		   ROS_INFO("[Assembly Station] RUN");
		   state_ = State::AS_RUN;
	     }
	    break;
	case State::AS_RUN:

		if (getState().evacuated) {

		   outputs.LOT_Assemble = true;
		   outputs.CP1 = true;
		   outputs.CP2 = true;
		   outputs.CP3 = true;
}
		   sendOuputs(outputs);

		   ROS_INFO("[Assembly Station] STOP");
		   state_ = State::AS_STOP;
	
		break;
		}

	sendCommands(commands);
}

	private:
		enum class State { AS_STOP, AS_CHECK, AS_RUN };

		State state_;
};

int main(int argc, char* argv[]) {

    ros::init(argc,argv, "Assembly_Station");

    ros::NodeHandle node;

    ros::Rate loop_rate(50); // 50 Hz

    AssemblyStation assembly(node);

   while (ros::ok()) {
   assembly.process();

   ros::spinOnce();

   loop_rate.sleep();
   }
}
